# runner-images

## Description

GitHub Actions and Azure DevOps Agent VM image repository. This repository combines OS image releases from source: https://github.com/actions/runner-images.

## Supported Operating Systems

* Ubuntu 22

## Adding new OS images

Add images from upstream as Git submodule like so:

```bash
git submodule add -b releases/ubuntu22/20230611 https://github.com/actions/runner-images.git ubuntu22
```

This way you can have different releases of different OS images in this same repository.
NOTE: There will be a bit of overhead from scripts and so on but this way you can manage the releases independently.

